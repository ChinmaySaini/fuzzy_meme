from setuptools import setup, find_packages

setup(
    name="data-science-local",
    version="17.0",
    description="Structure testing",
    author="Chinmay Swaroop Saini",
    packages=find_packages(),
)

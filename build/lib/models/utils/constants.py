LISTING_STATUS_CHOICES = (
    ('incomplete', 'incomplete'),
    ('inactive', 'inactive'),
    ('active', 'active'),
    ('expired', 'expired'),
    ('truebiled', 'truebiled'),
    ('booked', 'booked'),
    ('soldByOthers', 'soldByOthers'),
    ('requestedInspection', 'requestedInspection'),
    ('refurbishing', 'refurbishing')
)
